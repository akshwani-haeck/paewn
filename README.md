# PAEWN #



### What is PAEWN? ###

**PAEWN** (**P**ostman **AE**ternity **W**orkspace **N**exus) is a public workspace consisting of 
**aeternity** SDKs (**JavaScript** , **Recheck.io**), APIs (**Node HTTP**, **JavaScript** frontend, **AeMdw** backend), documentation, and web apps.